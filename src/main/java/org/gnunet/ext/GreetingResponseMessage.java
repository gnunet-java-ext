package org.gnunet.ext;

import org.gnunet.construct.UnionCase;
import org.gnunet.construct.ZeroTerminatedString;
import org.gnunet.util.GnunetMessage;

/**
 * Response to a greeting request from a client, sent by the server if name is not banned.
 *
 * @author Florian Dold
 */
@UnionCase(43002)
public class GreetingResponseMessage implements GnunetMessage.Body {
    @ZeroTerminatedString
    public String greeting;
}
