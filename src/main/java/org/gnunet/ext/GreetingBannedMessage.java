package org.gnunet.ext;

import org.gnunet.construct.UnionCase;
import org.gnunet.construct.ZeroTerminatedString;
import org.gnunet.util.GnunetMessage;

/**
 * Message sent by the server when the requested name for the greeting is banned.
 *
 * @author Florian Dold
 */
@UnionCase(43003)
public class GreetingBannedMessage implements GnunetMessage.Body {
    @ZeroTerminatedString
    public String banText;
}
