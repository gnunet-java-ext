package org.gnunet.ext;

import org.gnunet.construct.Union;
import org.gnunet.construct.UnionCase;
import org.gnunet.construct.ZeroTerminatedString;
import org.gnunet.util.GnunetMessage;

/**
 * Message to request a greeting from the greeting server.
 *
 * @author Florian Dold
 */
@UnionCase(43001)
public class GreetingRequestMessage implements GnunetMessage.Body {
    @ZeroTerminatedString
    public String name;
}
