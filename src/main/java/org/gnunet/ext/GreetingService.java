package org.gnunet.ext;

import org.gnunet.util.RelativeTime;
import org.gnunet.util.Server;
import org.gnunet.util.Service;
import org.gnunet.util.getopt.Argument;
import org.gnunet.util.getopt.ArgumentAction;

import java.util.*;

/**
 * Simple service that responds to greeting requests
 * with a user-defined greeting.
 *
 * @author Florian Dold
 */
public class GreetingService {

    public static void main(String... argv) {
        new Service("greeting", RelativeTime.FOREVER, true) {
            @Argument(
                action = ArgumentAction.STORE_STRING,
                argumentName = "GREETING_TEMPLATE",
                description = "Template for the greeting, '%s' is substituted for the name!",
                shortname = "g",
                longname = "greeting")
            String greetingTemplate = "Hello, %s";

            @Override
            public void run() {
                final String bannedStr = getConfiguration().getValueString("greeting", "BANNED").or("");
                final String banMessage = getConfiguration().getValueString("greeting", "BAN_MESSAGE").or("banned");
                final List<String> banned;
                if (bannedStr != null) {
                    banned = Arrays.asList(bannedStr.split(";"));
                } else {
                    banned = Collections.emptyList();
                }

                getServer().setHandler(new Server.MessageRunabout() {
                    public void visit(GreetingRequestMessage rm) {
                        if (banned.contains(rm.name.trim())) {
                            GreetingBannedMessage m = new GreetingBannedMessage();
                            m.banText = banMessage == null ? "" : banMessage.replace("%s", rm.name);
                            getSender().transmitWhenReady(RelativeTime.FOREVER, m, null);
                        } else {
                            GreetingResponseMessage m = new GreetingResponseMessage();
                            m.greeting = greetingTemplate.replace("%s", rm.name);
                            getSender().transmitWhenReady(RelativeTime.FOREVER, m, null);
                        }
                        getSender().receiveDone(true  );
                    }
                });

            }
        }.start(argv);
    }
}

