package org.gnunet.ext;

import org.gnunet.util.*;
import org.gnunet.util.getopt.Argument;
import org.gnunet.util.getopt.ArgumentAction;

/**
 * ...
 *
 * @author Florian Dold
 */
public class GreetingClient {
    public static void main(String[] args) {
        new Program() {
            @Argument(
                    action = ArgumentAction.STORE_STRING,
                    argumentName = "name",
                    description = "name to greet",
                    shortname = "n",
                    longname = "name")
            String name;

            public void run() {
                if (name == null) {
                    System.out.println("no name given");
                    System.exit(2);
                }
                final Client client = new Client("greeting", getConfiguration());
                GreetingRequestMessage m = new GreetingRequestMessage();
                m.name = name;
                client.transmitWhenReady(RelativeTime.SECOND, m, new Continuation() {
                    @Override
                    public void cont(boolean success) {
                        if (success) {
                            client.receiveOne(RelativeTime.SECOND, new RunaboutMessageReceiver() {
                                public void visit(GreetingResponseMessage m) {
                                    System.out.println("you were greeted:");
                                    System.out.println(m.greeting);
                                }
                                public void visit(GreetingBannedMessage m) {
                                    System.out.println("you are banned:");
                                    System.out.println(m.banText);
                                }
                                @Override
                                public void handleError() {
                                    System.out.println("could not receive from service");
                                    System.exit(1);
                                }
                            });
                        } else {
                            System.out.println("could not send to service");
                            System.exit(1);
                        }
                    }
                });
            }
        }.start(args);
    }
}
